---
layout: post
title:  "Can I submit content on sissyasstrainer?"
author: veronica
categories: [ feminization, contribution ]
image: assets/images/share-content-how-to-2.jpg
featured: true
hidden: true
toc: true
---


## Overview: Sharing content on sissyasstrainer

As we say - sharing is caring!

There's nothing better 
I knew you would come back for more. You crave it. It's stronger than you. Every time you think about it, your little heart starts pounding stronger in your chest.

We cannot sustain a community without contributing to it.


Sharing content is one of the first steps a sissy should commit to once taking on the inevitable path of understanding one's true self.

This is the first line you should cross. This is your true rebirth. 

Perhaps you've posted some original content on tumblr or bdsmlr (well we all did, duh!). Regardless of that, presisting content on sissyasstrainer is <b>bound with certain rules.</b>

## Sharing personal pictures, videos and articles

You can share your original content - photos, pictures, videos or an article that you've written by sending an email to <span class="spoiler"><b>veronica@sissyasstrainer.com</b></span>

### An important thing about contributions

When you share content, you should include: 
````html
[CONTRIBUTION]
````
as the beginning of an email topic.

Emails that have attached files, but lack the exact email topic prefix - like the one above - will be not prioritized as high as other ones.

## Not every post will be included

Bare in mind that even if you have submitted an email with attached files, I will follow my blog development strategy and your article or other media may be included at a later stage. 
The fact that your content wasn't included in the first few days does not mean that it is discarded.

## Every contribution should contain a source

I will include the source of a media (pictures, videos or articles) by posting a user name that has been provided in the email.
I will not include any media that does not contain a source. 
If you've created a modification of an image, you need to provide the exact source of information.
The same goes for articles and videos. 

## How does a contribution email look like

A valid contribution email should look something like this:

````html
from: sampleuser@sampledomain.com
to: 
topic: [CONTRIBUTION] Picture of ... from the day before / last week
email body:
      Hey, this is my picture / the picture of... 
      / from my personal profile / tinder / etc. 
      I've uploaded it on ... / I still have not uploaded it anywhere,
      but I'd like to include it in sissyasstrainer. 
      My tumblr/bdsmlr/twitter/etc username is sampleuser.
````

The template above will give you a basic starting point.


Well, it seems that that's the end of the line.

In the meantime, if you've got something on your mind, do not hesitate to drop an email - <span class="spoiler"><b>veronica@sissyasstrainer.com</b></span> 

